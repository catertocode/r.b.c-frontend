import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility,
  Label,
} from 'semantic-ui-react'
import '../css/index.css';
import myImage from '../about.png';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'

export default class About extends Component {
  state = {}

  render() {
    const { visible } = this.state

    return (
        <div>

          <Visibility
            onBottomPassed={this.showFixedMenu}
            onBottomVisible={this.hideFixedMenu}
            once={false}
          >

          <Segment inverted
            textAlign='center'
            style={{ padding: '1em 0em' }}
            vertical
          >

          <Container text>
            <Header
                as='h1'
                id='headerTextFadeIn'
                content='About Us'
                inverted
                style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: '1em', marginTop: '0em' , fontFamily: 'Baskerville Old Face'}}
            />
          </Container>
          </Segment>
          </Visibility>

            <Container text> <br/>
            <Grid divided='vertically'>
              <Grid.Row columns={2}>
                <Grid.Column> <Image src={ myImage } /></Grid.Column>
                <Grid.Column>Never trust a skinny cook! Ron and Jeff have been besties for like forever.
                    They've been cooking together for so long that they decided to make a buisness out of it.
                    They specialize in Louisiana cuisine but can cook just about anything!</Grid.Column>
              </Grid.Row>
            </Grid>
            </Container>



      </div>
        
    )
  }
}