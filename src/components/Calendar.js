import React, { Component } from 'react'
import '../css/index.css';
import DayPicker from 'react-day-picker';
import '../css/calendar.css';
import { Link } from "react-router-dom";
import ReactDOM from 'react-dom'
//import Order from './Order.js'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Transition,
  List,
  Menu,
  Message,
  Segment,
  Visibility,
  Dropdown,
  Label, 
} from 'semantic-ui-react'

export default class Calendar extends React.Component {
    state = {selectedDay: new Date(),};
    
    handleDayClick = (day, {disabled, selected}) => {
      if (selected) {
        // Unselect the day if already selected
        this.setState({ selectedDay: undefined });
        return;
      }
      if (disabled) {
        return;
      }
      this.setState({ 
        selectedDay: day 
      });
    };
    render(){
    return (
        <div>
            <center>

                <Visibility
                    onBottomPassed={this.showFixedMenu}
                    onBottomVisible={this.hideFixedMenu}
                    once={false}
                >

                <Segment
                    inverted
                    textAlign='center'
                    style={{ padding: '1em 0em' }}
                    vertical
                >
                <Container text>
                <Header
                    as='h1'
                    id='headerTextFadeIn'
                    content='Availability Calendar'
                    inverted
                    style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: '1em', marginTop: '0em' , fontFamily: 'Baskerville Old Face'}}
                />
                </Container>

                </Segment>
                </Visibility>

                <br/>

            <DayPicker
          onDayClick={this.handleDayClick}
          disabledDays={[
            new Date(2017, 12, 25),
            new Date(2017, 12, 14),
          ]}
        />
        <div id = 'calendar'>
          This calendar shows our available days. The days in gray are unavailable. <br/><br/>
          
          </div>
            </center>
          </div>
    )
              }} 