import React, { Component } from 'react'
import {
  Button,
  Container,
  Checkbox,
  Divider,
  Form,
  Grid,
  Header,
  Icon,
  Image,
  Input,
  List,
  Menu,
  Message,
  Segment,
  Visibility,
  Label,
} from 'semantic-ui-react'
import '../css/index.css';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'

export default class Contact extends Component {
  state = {}
  
  handleChange = (e, { name, value }) => this.setState({ [name]: value })
  handleSubmit = () => this.setState({ email: '', name: '', phone: '', message: '' })
 
  render() {

    const { name, email, phone, message } = this.state

    return (
        <div>

        <Visibility
          onBottomPassed={this.showFixedMenu}
          onBottomVisible={this.hideFixedMenu}
          once={false}
        >
        <Segment
          inverted
          textAlign='center'
          style={{ padding: '1em 0em' }}
          vertical
        >

        <Container text>
          <Header
              as='h1'
              id='headerTextFadeIn'
              content='Contact'
              inverted
              style={{ fontSize: '4em', fontWeight: 'normal', marginBottom: '1em', marginTop: '0em' , fontFamily: 'Baskerville Old Face'}}
           />
              
        </Container>

        </Segment>
        </Visibility>

        <Container text> <br/>
          <Header as='h2'>We'd love to hear from you</Header>
            <p>Thank you for your interest in Roux Bayou!</p>
            <p>Get in touch with us for your event inquiries!
              Please complete the form below with your name, email, and phone number to
              send us a message and we'll respond as soon as possible.</p>
              <br/>
    
          <Form onSubmit={this.handleSubmit}>
            <Form.Group widths='equal'>
              <Form.Input label='' placeholder='Name' name='name' value={name} onChange={this.handleChange} />
              <Form.Input label='' placeholder='Phone Number: (###) ###-####' name='phone' value={phone} onChange={this.handleChange} />
            </Form.Group>

            <Form.Field>
              <Form.Input label='' placeholder='Email' name='email' value={email} onChange={this.handleChange} />
            </Form.Field>

          <Form.Field>
            <Form.TextArea label='' placeholder='Message' name='message' value={message} onChange={this.handleChange} />
          </Form.Field>
    
          <center>
            <Button type='submit'>Submit</Button> <br/><br/>
          </center>

          </Form>

        </Container>
        
        </div>
    )
  }
}