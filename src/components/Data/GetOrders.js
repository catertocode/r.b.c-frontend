import React, { Component } from 'react';

const urlForOrder = orderID =>
  `http://localhost:5000/api/order/${orderID}`
 

class GetOrders extends Component {
  constructor(props) {
    super(props)
    this.state = {
      requestFailed: false
    }
  }

  componentDidMount() {
    fetch(urlForMenu(this.props.orderID))
      .then(response => {
        if (!response.ok) {
          throw Error("Network request failed")
        }

        return response
      })
      .then(d => d.json())
      .then(d => {
        this.setState({
          OrderData: d
        })
      }, () => {
        this.setState({
          requestFailed: true
        })
      })
  }

  render() {

    if (this.state.requestFailed) return <p>Nothing Here Yet</p>
    if (!this.state.OrderData) return <p>Loading...</p>
    return (
      <div>
        <h4>{this.state.OrderData.name}</h4>
        <h4>{this.state.OrderData.adress}</h4>
        <h4>{this.state.OrderData.city}</h4>
        <h4>{this.state.OrderData.state}</h4>
        <h4>{this.state.OrderData.date}</h4>
        <h4>{this.state.OrderData.zip}</h4>
        <h4>{this.state.OrderData.orders}</h4>
        <h4>{this.state.OrderData.selectPackage}</h4>
        
      </div>
    )
  }
}


export default MenuAPI;