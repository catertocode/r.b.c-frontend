import React, { Component } from 'react';

const urlForMenu = menuID =>
  `http://localhost:5000/api/menu/${menuID}`
 

class MenuAPI extends Component {
  constructor(props) {
    super(props)
    this.state = {
      requestFailed: false
    }
  }

  componentDidMount() {
    fetch(urlForMenu(this.props.menuID))
      .then(response => {
        if (!response.ok) {
          throw Error("Network request failed")
        }

        return response
      })
      .then(d => d.json())
      .then(d => {
        this.setState({
          MenuData: d
        })
      }, () => {
        this.setState({
          requestFailed: true
        })
      })
  }

  render() {

    if (this.state.requestFailed) return <p>Nothing Here Yet</p>
    if (!this.state.MenuData) return <p>Loading...</p>
    return (
      <div >
        <table>
        <h2>{this.state.MenuData.menuType}</h2>
        <h4>{this.state.MenuData.menuName}</h4>
        <h4>{this.state.MenuData.menuDescription}</h4>
        <h4>{this.state.MenuData.menuPrice}</h4>
        </table>
      </div>
    )
  }
}


export default MenuAPI;

