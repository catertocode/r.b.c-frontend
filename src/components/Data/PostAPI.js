import React, { Component } from 'react';
import axios from 'axios';

class PostAPI extends Component{

    constructor(){
        super();
        this.state={
            menuName:'',
            menuDescription:'',
            menuPrice:''
        };
    }
    onChange = (e)=>{
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }

    onSubmit = (e) =>{
        e.preventDefault();
        const {menuName,menuDescription,menuPrice} = this.state;

        axios.post('http://localhost:5000/api/menu',{
            menuName,menuDescription,menuPrice,
            headers : {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin" : "http://localhost:5000/api/menu", 
                        "Access-Control-Allow-Credentials" : true
         }     
        })
            .then((result)=>{

            });
    }
    render(){
        const {menuName,menuDescription,menuPrice}= this.state;
        return(
            <form onSubmit={this.onSubmit}>
            <input type="text" placeholder="Item Name" name="menuName" value={menuName} onChange={this.onChange}/><br/>
            <input type="text" placeholder="Item Price" name="menuPrice" value={menuPrice} onChange={this.onChange}/><br/>
            <input type="text" placeholder="Description" name="menuDescription" value={menuDescription} onChange={this.onChange}/><br/>
            <button type= "submit" >Create</button>
            </form>
        );
    }
}

export default PostAPI;