import React, { Component } from 'react';
import axios from 'axios';


class PostOrder extends Component{

    constructor(){
        super();
        this.state={
            name:'',
            address:'',
            city:'',
            state:'',
            zip:'',
            selectPackage:'',
            date:'',
            orders:'',
            email:'',
            phone:'',
            entreeOne:'',
            entreeTwo:'',
            entreeThree:'',
            entreeFour:'',
            sideOne:'',
            sideTwo:'',
            sideThree:'',
            sideFour:'',
            sideFive:'',
            sideSix:''

        };
    }
    onChange = (e)=>{
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }
    onSubmit = (e) =>{
        e.preventDefault();
        const {name,address,city,state,zip,selectPackage,date,orders,email,phone,entreeOne,
            entreeTwo,
            entreeThree,
            entreeFour,
            sideOne,
            sideTwo,
            sideThree,
            sideFour,
            sideFive,
            sideSix,} = this.state;

        axios.post('http://localhost:5000/api/order',{
            name,address,city,state,zip,selectPackage,date,orders,email,phone,entreeOne,
            entreeTwo,
            entreeThree,
            entreeFour,
            sideOne,
            sideTwo,
            sideThree,
            sideFour,
            sideFive,
            sideSix,
            headers : {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin" : "http://localhost:5000/api/order", 
                        "Access-Control-Allow-Credentials" : true
         }     
        })
            .then((result)=>{

            });
    }
    render(){
        const {name,address,city,state,zip,selectPackage,date,email,phone,
            entreeOne,
            entreeTwo,
            entreeThree,
            entreeFour,
            sideOne,
            sideTwo,
            sideThree,
            sideFour,
            sideFive,
            sideSix,}= this.state;
        return(
            <form onSubmit={this.onSubmit}>
                <input placeholder="Name" name="name" value={name} onChange={this.onChange}/><br/>
                <input placeholder="Street Address" name="address" value={address} onChange={this.onChange}/><br/>
                <input placeholder="Email" name="email" type="email" value={email} onChange={this.onChange}/><br/>
                <input placeholder="Phone Number" name="phone" type="tel" value={phone} onChange={this.onChange}/><br/>
                <input placeholder="City" name="city" value={city} onChange={this.onChange}/><br/>
                <input placeholder="State" name="state" value={state} onChange={this.onChange}/><br/>
                <input placeholder="Zip" name="zip" value={zip} onChange={this.onChange}/><br/>
                <h2>Select Entrees</h2>
                {/*<MenuAPI  menuID="2-13"> </MenuAPI>*/}
                <h2>Select Sides/Appetizers</h2>
                {/*MenuAPI 14-23*/}
               {/* <input placeholder="Package" name="selectPackage" value={selectPackage} onChange={this.onChange}/><br/>
                <input placeholder="Date" min='2017-11-11' type="date"name="date" value={date} onChange={this.onChange}/><br/>
                {/*<input placeholder="Order" name="orders" value={orders} onChange={this.onChange}/><br/>*/}
                <button type= "submit" >Place Order</button>
            </form>
            );
            }onSubmit={this.onSubmit}

}

export default PostOrder;