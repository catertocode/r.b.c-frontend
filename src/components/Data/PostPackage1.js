import React, { Component } from 'react';
import axios from 'axios';
import {
    Button,
    Container,
    Divider,
    Grid,
    Header,
    Icon,
    Image,
    List,
    Menu,
    Segment,
    Visibility,
    Label,
  } from 'semantic-ui-react'

class PostPackage1 extends Component{

    constructor(){
        super();
        this.state={
            name:'',address:'',city:'',state:'',zip:'',date:'',orders:'',email:'',
            phone:'',entreeOne:'',entreeTwo:'',entreeThree:'',entreeFour:'',sideOne:'',
            sideTwo:'',sideThree:'',sideFour:'',sideFive:'',sideSix:''
        };
    }
    onChange = (e)=>{
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }
    onSubmit = (e) =>{
        e.preventDefault();
        const {name,address,city,state,zip,date,orders,email,phone,entreeOne,
            entreeTwo,entreeThree,entreeFour,sideOne,sideTwo,
            sideThree,sideFour,sideFive,sideSix,} = this.state;

        axios.post('http://localhost:5000/api/orders',{
            name,address,city,state,zip,date,orders,email,phone,entreeOne,entreeTwo,
            entreeThree,entreeFour,sideOne,sideTwo,sideThree,sideFour,sideFive,sideSix,
            headers : {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin" : "http://localhost:5000/api/orders", 
                        "Access-Control-Allow-Credentials" : true
         }     
        })
            .then((result)=>{

            });
    }
    render(){
        const {name,address,city,state,zip,date,email,phone,entreeOne,entreeTwo,
            entreeThree,entreeFour,sideOne,sideTwo,sideThree,sideFour,sideFive,
            sideSix,}= this.state;
        return(
            <form onSubmit={this.onSubmit}>
                <input placeholder="Name" name="name" value={name} onChange={this.onChange}/><br/>
                <input placeholder="Street Address" name="address" value={address} onChange={this.onChange}/><br/>
                <input placeholder="Email" name="email" type="email" value={email} onChange={this.onChange}/><br/>
                <input placeholder="Phone Number" name="phone" type="tel" value={phone} onChange={this.onChange}/><br/>
                <input placeholder="City" name="city" value={city} onChange={this.onChange}/><br/>
                <input placeholder="State" name="state" value={state} onChange={this.onChange}/><br/>
                <input placeholder="Zip" name="zip" value={zip} onChange={this.onChange}/><br/>
                <input placeholder="Date" min='2017-11-11' type="date"name="date" value={date} onChange={this.onChange}/><br/>
            <h2>Select Entrees</h2>
                <select name="entreeOne" onChange={this.onChange}>
                    <option value="Select">Select Entree </option>
                    <option value="Gumbo">Gumbo</option>
                    <option value="SeafoodGumbo">Seafood Gumbo</option>
                    <option value="Jambalaya">Jambalaya</option>
                    <option value="Shrimp Scampi pasta ">Shrimp Scampi pasta </option>
                    <option value="Pork roast">Pork roast</option>
                    <option value="Mini-Muffulettas">Mini-Muffulettas</option>
                    <option value="Pulled Pork">Pulled Pork</option>
                    <option value="Fried Catfish">Fried Catfish</option>
                </select>
            <h2>Select Sides/Appetizers</h2>
                    <select name="sideOne" onChange={this.onChange}>
                    <option value="Hush puppies">Hush puppies</option>
                    <option value="Spinach Dip">Spinach Dip (chips included)</option>
                    <option value="Macaroni cheese">Macaroni and Cheese</option>
                    <option value="Mini weenies ">Mini weenies</option>
                    <option value="Meatballs ">Meatballs </option>
            </select>
                <select name="sideTwo" onChange={this.onChange}>
                    <option value="Hush puppies">Hush puppies</option>
                    <option value="Spinach Dip">Spinach Dip (chips included)</option>
                    <option value="Macaroni cheese">Macaroni and Cheese</option>
                    <option value="Mini weenies ">Mini weenies</option>
                    <option value="Meatballs ">Meatballs </option>
                </select>  
                <button type= "submit" >Place Order</button>
            </form>
            );
            }
}
export default PostPackage1;