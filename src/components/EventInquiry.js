import React, { Component } from 'react'
//import '../css/index.css';
import DayPicker from 'react-day-picker';
import '../css/calendar.css';
import { Link } from "react-router-dom";
import ReactDOM from 'react-dom'

import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  Transition,
  List,
  Menu,
  Segment,
  Step,
  Visibility,
  Dropdown,
  Label,
} from 'semantic-ui-react'


export default class EventInquiry extends React.Component {
  state = {selectedDay: new Date(),};
  
  handleDayClick = (day, {disabled, selected}) => {
    if (selected) {
      // Unselect the day if already selected
      this.setState({ selectedDay: undefined });
      return;
    }
    if (disabled) {
      return;
    }
    this.setState({ 
      selectedDay: day 
    });
  };

  render() {
    

      return (

      <div>
        <center>

        <Visibility
            onBottomPassed={this.showFixedMenu}
            onBottomVisible={this.hideFixedMenu}
            once={false}
        >

        <Segment
            inverted
            textAlign='center'
            style={{ padding: '1em 0em' }}
            vertical
        >

      <Container>
          <center>
          <Step.Group unstackable>
                  <Step disabled>
                      <Icon name='add'/>
                      <Step.Content>
                          <Step.Title>Basic Information</Step.Title>
                          <Step.Description>Complete form with <p>required information.</p></Step.Description>
                      </Step.Content>
                  </Step>
                  <Step active>
                      <Icon name='calendar'/>
                      <Step.Content>
                          <Step.Title>Calendar</Step.Title>
                          <Step.Description>Select date of <p>your event</p></Step.Description>
                      </Step.Content>
                  </Step>
                  <Step disabled>
                      <Icon name='target' />
                      <Step.Content>
                          <Step.Title>Package</Step.Title>
                          <Step.Description>Choose desired package <p>and customize to your preference</p></Step.Description>
                      </Step.Content>
                  </Step>
                  <Step disabled>
                      <Icon name='info circle' />
                      <Step.Content>
                          <Step.Title>Confirm Order</Step.Title>
                          <Step.Description>Verify your order details <p>to ensure accuracy</p></Step.Description>
                      </Step.Content>
                  </Step>
              <Step disabled>
                  <Icon name='check'/>
                  <Step.Content>
                      <Step.Title>Order Placed</Step.Title>
                      <Step.Description>You have successfully placed <p>an order!</p></Step.Description>
                  </Step.Content>
              </Step>
              </Step.Group> <br/>
            </center>


      </Container>

      </Segment>
      </Visibility>


        <div id = 'calle'> <br/>
        <DayPicker
          onDayClick={this.handleDayClick}
          disabledDays={[
            new Date(2017, 9, 12),
            new Date(2017, 9, 2),
          ]}
        />

            <div id = 'calendar'>
                This calendar shows our available days. The days in gray are unavailable. <br/>

        <br/> <br/>

          Date selected: <br/>
        </div>

      <div id = 'date'>
        { this.state.selectedDay.toLocaleDateString(window.navigator.language, {weekday: 'long'})}, 
        &nbsp;
        { this.state.selectedDay.toLocaleDateString(window.navigator.language, {month: 'long'})}
        &nbsp;
        { this.state.selectedDay.toLocaleDateString(window.navigator.language, {day: 'numeric'})},
        &nbsp;
        { this.state.selectedDay.toLocaleDateString(window.navigator.language, {year: 'numeric'}) } 
      </div> <br/>

          <div hidden> {window.date = this.state.selectedDay.toLocaleDateString()} </div>
       <center>
         <Button animated color='blue' size='huge'>
           <Button.Content visible>
             <Link to='/order' style={{ fontWeight: 'bold' , color: 'white' }}>
               Confirm Date
             </Link>
           </Button.Content>
           <Button.Content hidden>
             <Link to='/order' style={{ fontWeight: 'bold' , color: 'white' }}>
               Select a package<Icon name='food'/>
             </Link>
           </Button.Content>
         </Button>
       </center>

        <br/>

        

      </div>
      </center>
      </div>

    )
  }
}
   