import React from "react";
import {Link} from "react-router-dom";
import {
    Button,
    Container,
    Divider,
    Grid,
    Header,
    Icon,
    Image,
    List,
    Menu,
    Segment,
    Visibility,
} from 'semantic-ui-react'

const Footer = () => (
    <div>

        <Segment inverted vertical style={{padding: '5em 0em'}}>
            <Container>
                <Grid divided inverted stackable>
                    <Grid.Row>
                        {/* 'About' footer section on the home page */}
                        <Grid.Column width={4}>
                            <List>
                                <List.Item>
                                    <List.Icon name='users' />
                                    <List.Content>Roux Bayou Catering</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='marker' />
                                    <List.Content>Hammond, LA</List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='mail' />
                                    <List.Content>
                                        <a href='mailto:rouxbayoucatering@gmail.com'>RouxBayouCatering@gmail.com</a>
                                    </List.Content>
                                </List.Item>
                                <List.Item>
                                    <List.Icon name='linkify' />
                                    <List.Content>
                                        <a href='http://www.rouxbayou.com'>rouxbayou.com</a>
                                    </List.Content>
                                </List.Item>
                            </List>
                        </Grid.Column>
                        {/* 'Services' footer section on the home page */}
                        <Grid.Column width={3}>
                            <Header inverted as='h4' content='Services'/>
                            <List link inverted>
                                <List.Item as='a'><Link to='/menu'>Menu</Link></List.Item>
                                <List.Item as='a'><Link to='/eventinquiry'>Check our Availability</Link></List.Item>
                                <List.Item as='a'>How to Order</List.Item>
                                <List.Item as='a'>FAQs</List.Item>
                            </List>
                        </Grid.Column>
                        {/* Social media footer section on the home page */}
                        <Grid.Column width={7}>
                            <Header as='h4' inverted>Keep up with Roux Bayou Catering!</Header>
                            <List link inverted>
                                <List.Item as='a'><a href="https://www.facebook.com/RouxBayouCatering/"> <Icon name='facebook square'/>Facebook</a></List.Item> {/* Roux Bayou's Facebook */}
                                <List.Item as='a'><a href="https://www.twitter.com/RouxBayou/"> <Icon name='twitter square'/> Twitter</a></List.Item> {/* Roux Bayou's Twitter */}
                            </List>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Container>
        </Segment>

    </div>
);

export default Footer;
