import React from "react";
import '../css/header.css';
import { Link } from "react-router-dom";
import {
  Button,
  Container,
  Divider,
  Grid,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility,
} from 'semantic-ui-react'

// The Header creates links that can be used to navigate
// between routes.

const Header = () => (
    <div>

      <div id="pagesHeader">
        <Menu inverted pointing secondary size='large'   position='right' >
          <a><Link to="/" style={{ textDecoration: 'none' }} id="linkd">Roux Bayou Catering</Link> </a>
          <a><Link to="/about" style={{ textDecoration: 'none' }} id="linkd">ABOUT</Link></a>
          <a><Link to="/menu" style={{ textDecoration: 'none' }} id="linkd">MENU</Link></a>
          <a><Link to="/calendar" style={{ textDecoration: 'none' }} id='linkd'>CALENDAR</Link></a>
          <a><Link to="/orderWelcome" style={{ textDecoration: 'none' }} id="linkd">ORDER</Link></a>
          <a><Link to="/contact" style={{ textDecoration: 'none' }} id="linkd">CONTACT</Link></a>
        </Menu>
      </div>

    </div>
);

export default Header;
