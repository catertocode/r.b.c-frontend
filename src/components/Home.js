import React, { Component } from 'react'
import {
  Button,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Visibility,
  Label,
} from 'semantic-ui-react'
import myImage from '../rouxBayouLogo.png'; // Home page's header image import
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'

export default class HomepageLayout extends Component {
  state = {}

  render() {

    const { visible } = this.state

    return (
        <div>

        <Visibility
          onBottomPassed={this.showFixedMenu}
          onBottomVisible={this.hideFixedMenu}
          once={false}
        >

        <Segment
            inverted
            textAlign='center'
            style={{ padding: '1em 0em' }}
            vertical
        >

        <Container>

           <Image src={ myImage } />  {/* Home page's Roux Bayou header image */}
          
           <Header
               as='h2'
               content='Ron Abel and Jeff Garland'
               inverted
               style={{ fontSize: '3em', fontWeight: 'normal' , fontFamily: 'Baskerville Old Face'}}
           />

          <Header
              as='h2'
              content='Located in Hammond, LA'
              inverted
              style={{ fontSize: '3em', fontWeight: 'normal' , marginBottom: '1em', fontFamily: 'Baskerville Old Face'}}
          />

          {/* 'Have an Event? Order Now' button on the home page */}
          <Button animated='fade' basic inverted size = 'huge' onClick = {this.EventInquiry}>
          <Button.Content visible><Link to='/orderwelcome'style={{ fontWeight: 'bold' , color: 'white', }}> Have an Event? </Link></Button.Content>
            <Button.Content hidden><Link to='/orderwelcome'style={{ fontWeight: 'bold' , color: 'white' }}> Order Now </Link></Button.Content>
           
          </Button>

        </Container>
        </Segment>
        </Visibility>

        </div>
    ) /* End 'return' */

  } /* End 'render' */

} /* End 'export... Container' */