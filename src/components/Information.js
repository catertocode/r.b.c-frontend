import React, { Component } from 'react'
import {
    Button,
    Container,
    Checkbox,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Image,
    Input,
    List,
    Menu,
    Message,
    Segment,
    Step,
    Visibility,
    Label,
} from 'semantic-ui-react'
import '../css/index.css';
import '../css/calendar.css';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'
import axios from 'axios';
window.email
window.phone
window.location1
window.name
window.date
window.entree1
window.side1
window.totalCost = 0
global.entree = []
global.side = []
window.number
window.requests
window.pay
export default class Information extends Component {
    state = {}

    handleChange = (e, { name, value }) => this.setState({ [name]: value })
        
    handleSubmit = () => {
        this.setState({ email: '', name: '', phone: '', location: '' })
    }
    changeName = (e) => {
        this.setState({name: e.target.value })
        window.name = e.target.value;
    }
    changeEmail = (e) => {
        this.setState({email: e.target.value })
        window.email = e.target.value;
    }
    changeLocation1 = (e) => {
        this.setState({location: e.target.value })
        window.location1 = e.target.value;
    }
    changePhone = (e) => {
        this.setState({phone: e.target.value })
        window.phone = e.target.value;
    }
    render() {

        const { name, email, phone, location } = this.state

        return (
            <div>

                <Visibility
                    onBottomPassed={this.showFixedMenu}
                    onBottomVisible={this.hideFixedMenu}
                    once={false}
                >
                    <Segment
                        inverted
                        textAlign='center'
                        style={{ padding: '1em 0em' }}
                        vertical
                    >

                <Container>
                    <center>
                                <Step.Group unstackable>
                                    <Step active>
                                        <Icon name='add'/>
                                        <Step.Content>
                                            <Step.Title>Basic Information</Step.Title>
                                            <Step.Description>Complete the form with <p>the required information.</p></Step.Description>
                                        </Step.Content>
                                    </Step>
                                    <Step disabled>
                                        <Icon name='calendar'/>
                                        <Step.Content>
                                            <Step.Title>Calendar</Step.Title>
                                            <Step.Description>Select date of <p>your event</p></Step.Description>
                                        </Step.Content>
                                    </Step>
                                    <Step disabled>
                                        <Icon name='target' />
                                        <Step.Content>
                                            <Step.Title>Package</Step.Title>
                                            <Step.Description>Choose desired package <p>and customize to your preference</p></Step.Description>
                                        </Step.Content>
                                    </Step>
                                    <Step disabled>
                                        <Icon name='info circle' />
                                        <Step.Content>
                                            <Step.Title>Confirm Order</Step.Title>
                                            <Step.Description>Verify your order details <p>to ensure accuracy</p></Step.Description>
                                        </Step.Content>
                                    </Step>
                                    <Step disabled>
                                        <Icon name='check'/>
                                        <Step.Content>
                                            <Step.Title>Order Placed</Step.Title>
                                            <Step.Description>You have successfully placed <p>an order!</p></Step.Description>
                                        </Step.Content>
                                    </Step>
                                </Step.Group> <br/>
                    </center>
                        </Container>

                    </Segment>
                </Visibility>
<div id = 'package'>
                <Container text> <br/>
                    <div id = 'orderHeader'>
                        COMPLETE THE FOLLOWING FORM</div> <br/>
        <div id='orderDescriptions'>
                            <p> Please provide your name, email address, phone number, and the
                                location of your event in the form below. </p></div>

                    <br/>

                    <Form onSubmit={this.handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input label='' placeholder='Name' name='name' value={name} onChange={this.changeName} />
                            <Form.Input label='' placeholder='Phone Number: (###) ###-####' name='phone' value={phone} onChange={this.changePhone} />
                        </Form.Group>

                        <Form.Field>
                            <Form.Input label='' placeholder='Email' name='email' value={email} onChange={this.changeEmail} />
                        </Form.Field>

                        <Form.Field>
                            <Form.Input label='' placeholder='City, State of your event' name='location' value={location} onChange={this.changeLocation1} />
                        </Form.Field>

                        <br/>

                        <center>
                            <Button animated color='blue' size='huge' >
                                <Button.Content visible>
                                    <Link to='/eventInquiry' style={{ fontWeight: 'bold' , color: 'white' }}>
                                        Complete
                                    </Link>
                                </Button.Content>

                                <Button.Content hidden>
                                    <Link to='/eventInquiry' style={{ fontWeight: 'bold' , color: 'white' }}>
                                        Select Date <Icon name='checked calendar'/>
                                    </Link>
                                </Button.Content>
                            </Button>
                        </center>

                        <br/>
                       
                    </Form>

                </Container>
</div>
            </div>
        )
    }
}