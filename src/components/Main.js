import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home'
import About from './About'
import Menu from './Menu'
import orderWelcome from './orderWelcome'
import Information from './Information'
import EventInquiry from './EventInquiry'
import orderConfirmation from './orderConfirmation'
import Contact from './Contact'
import Order from './Order'
import Calendar from './Calendar'
import Submit from './Submit'
import PostPackage1 from './Data/PostPackage1'


// The Main component renders one of the three provided
// Routes (provided that one matches). 

// Example: the /About and /Menu routes will match any 
// pathname that starts with /About or /Menu. The / route 
// will only match when the pathname is exactly the string "/"

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/about' component={About}/>
      <Route path='/menu' component={Menu}/>
      <Route path='/eventinquiry' component={EventInquiry}/>
      <Route path='/orderwelcome' component={orderWelcome}/>
      <Route path='/information' component={Information}/>
      <Route path='/orderconfirmation' component={orderConfirmation}/>
      <Route path='/contact' component={Contact}/>
      <Route path='/order' component={Order}/>
      <Route path='/calendar' component={Calendar}/>
      <Route path='/submit' component={Submit}/>
      <Route path='/postpackage1' component={PostPackage1}/>

    </Switch>

  </main>
)

export default Main