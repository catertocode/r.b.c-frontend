import React, { Component } from 'react'
//import '../css/index.css';
import DayPicker from 'react-day-picker';
import '../css/calendar.css';
import '../css/index.css'
import { Link } from "react-router-dom";
import ReactDOM from 'react-dom'
import MenuAPI from './Data/MenuAPI.js';
import PostAPI from './Data/PostAPI.js';
import axios from 'axios';
import {
    Radio,
  Button,
  Form,
  Container,
  Divider,
  Grid,
  Header,
  Icon,
  Input,
  Image,
  Transition,
  List,
  Menu,
  Segment,
  Step,
  Visibility,
  Dropdown,
  Label, 
} from 'semantic-ui-react'

const entrees = ['a', 'ch', 'chr', 'dani', 'ellio', 'helenn']
const sides = ['a', 'ch', 'chr', 'dani', 'ellio', 'helenn']
const entreesO = [{value:'Gumbo', text:<MenuAPI  menuID="2"> </MenuAPI>}, {value:<MenuAPI  menuID="3"> </MenuAPI>, text:<MenuAPI  menuID="3"> </MenuAPI>},{value:<MenuAPI  menuID="4"> </MenuAPI>, text:<MenuAPI  menuID="4"> </MenuAPI>}, {value:<MenuAPI  menuID="5"> </MenuAPI>, text:<MenuAPI  menuID="5"> </MenuAPI>}, {value:<MenuAPI  menuID="6"> </MenuAPI>, text:<MenuAPI  menuID="6"> </MenuAPI>},{value:<MenuAPI  menuID="7"> </MenuAPI>, text:<MenuAPI  menuID="7"> </MenuAPI>},{value:<MenuAPI  menuID="8"> </MenuAPI>, text:<MenuAPI  menuID="8"> </MenuAPI>}, {value:<MenuAPI  menuID="9"> </MenuAPI>, text:<MenuAPI  menuID="9"> </MenuAPI>},{value:<MenuAPI  menuID="10"> </MenuAPI>, text:<MenuAPI  menuID="10"> </MenuAPI>},{value:<MenuAPI  menuID="11"> </MenuAPI>, text:<MenuAPI  menuID="11"> </MenuAPI>}, {value:<MenuAPI  menuID="12"> </MenuAPI>, text:<MenuAPI  menuID="12"> </MenuAPI>},{value:<MenuAPI  menuID="13"> </MenuAPI>, text:<MenuAPI  menuID="13"> </MenuAPI>},{value:<MenuAPI  menuID="14"> </MenuAPI>, text:<MenuAPI  menuID="14"> </MenuAPI>}, {value:<MenuAPI  menuID="15"> </MenuAPI>, text:<MenuAPI  menuID="15"> </MenuAPI>}, {value:<MenuAPI  menuID="16"> </MenuAPI>, text:<MenuAPI  menuID="16"> </MenuAPI>}]
const sidesO = [{value:<MenuAPI  menuID="17"> </MenuAPI>, text:<MenuAPI  menuID="17"> </MenuAPI>},{value:<MenuAPI  menuID="18"> </MenuAPI>, text:<MenuAPI  menuID="18"> </MenuAPI>}, {value:<MenuAPI  menuID="19"> </MenuAPI>, text:<MenuAPI  menuID="19"> </MenuAPI>},{value:<MenuAPI  menuID="20"> </MenuAPI>, text:<MenuAPI  menuID="20"> </MenuAPI>},{value:<MenuAPI  menuID="21"> </MenuAPI>, text:<MenuAPI  menuID="21"> </MenuAPI>}, {value:<MenuAPI  menuID="22"> </MenuAPI>, text:<MenuAPI  menuID="22"> </MenuAPI>},{value:<MenuAPI  menuID="23"> </MenuAPI>, text:<MenuAPI  menuID="23"> </MenuAPI>},{value:<MenuAPI  menuID="24"> </MenuAPI>, text:<MenuAPI  menuID="24"> </MenuAPI>}, {value:<MenuAPI  menuID="25"> </MenuAPI>, text:<MenuAPI  menuID="25"> </MenuAPI>}, {value:<MenuAPI  menuID="26"> </MenuAPI>, text:<MenuAPI  menuID="26"> </MenuAPI>}]
var selectionsE = []
var selectionsS = []
var packageNum
var totalCost
var x = 0
var y = 0
var people = 10
var requests = 'none'

export default class Order extends React.Component {
  state = { visible1: false,  visible: true, items: entrees.slice(0, 3), items1: sides.slice(0, 3)}  
  
  pack1 = () =>{
    this.packageNum = 1;
    this.packageVal = 12;
     this.setState({ items: entrees.slice(0, 1) })
     this.setState({ items1: sides.slice(0, 2) }) 
    return this.setState({ visible1: !this.state.visible1 , visible: !this.state.visible })  
   }
   pack2 = () =>{
    this.packageNum = 2;    
    this.packageVal = 22;    
    this.setState({ items: entrees.slice(0,  2) })
    this.setState({ items1: sides.slice(0, 4) }) 
   return this.setState({ visible1: !this.state.visible1 , visible: !this.state.visible })  
  }
  pack3 = () =>{
    this.packageNum = 3; 
    this.packageVal = 32;    
    this.setState({ items: entrees.slice(0, 4) })
    this.setState({ items1: sides.slice(0,6) }) 
   return this.setState({ visible1: !this.state.visible1 , visible: !this.state.visible })  
  }
  handleChangeE = (value, key) => {
      global.entree[x] = value;
        x++; 
        if (x ==1){
            window.entree1 = value  
        }
        else{
        window.entree1 = window.entree1 + ', ' + value  
        } 
    }
  handleChangeS = (value, key) => {
      global.side[y-1] = value;
      y++;
      if (y ==1){
        window.side1 = value  
    }
    else{
    window.side1 = window.side1 + ', ' + value  
    } 
    }
  handleAdd = () =>{ this.setState({ items: entrees.slice(0, this.state.items.length + 1) }); this.packageVal = this.packageVal + 4  }
  handleAdd1 = () =>{ this.setState({ items1: sides.slice(0, this.state.items1.length + 1) }); this.packageVal = this.packageVal + 2 }
  changeNum = (e) => this.setState({people: e.target.value })
  addRequest = (e) => this.setState({requests: e.target.value })
  handleChange = (e, { value }) => this.setState({ value })
  
    render(){
      const { items, items1, visible1, visible2,  visible3, visible, packageNum, packageVal, selectEntree, selectSide, member } = this.state      
    return (
        <div>
        <Visibility
            onBottomPassed={this.showFixedMenu}
            onBottomVisible={this.hideFixedMenu}
            once={false}
        >

        <Segment
            inverted
            textAlign='center'
            style={{ padding: '1em 0em' }}
            vertical
        >

            <Container>
                <Step.Group unstackable>
                    <Step disabled>
                        <Icon name='add'/>
                        <Step.Content>
                            <Step.Title>Basic Information</Step.Title>
                            <Step.Description>Complete form with <p>required information.</p></Step.Description>
                        </Step.Content>
                    </Step>
                    <Step disabled>
                        <Icon name='calendar'/>
                        <Step.Content>
                            <Step.Title>Calendar</Step.Title>
                            <Step.Description>Select date of <p>your event</p></Step.Description>
                        </Step.Content>
                    </Step>
                    <Step active>
                        <Icon name='target' />
                        <Step.Content>
                            <Step.Title>Package</Step.Title>
                            <Step.Description>Choose desired package <p>and customize to your preference</p></Step.Description>
                        </Step.Content>
                    </Step>
                    <Step disabled>
                        <Icon name='info circle' />
                        <Step.Content>
                            <Step.Title>Confirm Order</Step.Title>
                            <Step.Description>Verify your order details <p>to ensure accuracy</p></Step.Description>
                        </Step.Content>
                    </Step>
                    <Step disabled>
                        <Icon name='check'/>
                        <Step.Content>
                            <Step.Title>Order Placed</Step.Title>
                            <Step.Description>You have successfully placed <p>an order!</p></Step.Description>
                        </Step.Content>
                    </Step>
                </Step.Group> <br/>


            </Container>

        </Segment>
        </Visibility>

     <div id = 'package'>
       <Transition.Group animation='fade up' duration={700}>
        {visible &&
        <div>
            <center>
            <Grid columns={3}>
                <Grid.Row>
                    <Grid.Column>
                        <Segment id = 'packageBoxes' size = 'huge' textAlign = 'center' raised color = 'blue'>
                            <b>Package 1<br/></b>
                            1 Entree <br/>
                            2 Sides <br/>
                            $$ per person <br/> <br/>
                        <Button color='blue' content={visible ? 'Select Package 1' : 'Show'} onClick={this.pack1} />
                        </Segment>
                    </Grid.Column>

                    <Grid.Column>
                        <Segment id = 'packageBoxes' size = 'huge' textAlign = 'center' raised color = 'blue'>
                            <b>Package 2<br/></b>
                            2 Entrees <br/>
                            4 Sides <br/>
                            $$ per person <br/> <br/>
                        <Button color = 'blue' content={visible ? 'Select Package 2' : 'Show'} onClick={this.pack2} />
                        </Segment>
                    </Grid.Column>

                    <Grid.Column>
                        <Segment id = 'packageBoxes' size = 'huge' textAlign = 'center' raised color = 'blue'>
                            <b>Package 3<br/></b>
                            4 Entrees <br/>
                            6 Sides <br/>
                            $$ per person <br/> <br/>
                        <Button color = 'blue' content={visible ? 'Select Package 3' : 'Show'} onClick={this.pack3} />
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            </center>
        </div>
        }

         {visible1 &&
         <div>
            <center>
            <Form onSubmit={this.onSubmit}>
             <Header
                 as='h1'
                 inverted
                 style={{ color: 'black', fontSize: '4em', fontWeight: 'normal', marginBottom: '1em', marginTop: '0em' , fontFamily: 'Baskerville Old Face'}}
             > Package {this.packageNum}
             <br/> Cost per person: $ {this.packageVal}
             </Header>
              <Container text>
            <Transition.Group 
                as={List}
                duration={200}
                divided
                size='large'
                verticalAlign='middle'
            >

            {items.map(item => (
            <List.Item key={item}>
             <Dropdown placeholder='Select Entree' value={member} fluid selection options = {entreesO} onChange={(e,{value})=>this.handleChangeE(value, 'member')} />  
             </List.Item>
          ))}

        </Transition.Group>
        <Button.Group>
          <Button compact disabled={items.length === entrees.length} onClick={this.handleAdd} ><Icon name='add circle'/>Add Entree</Button>
        </Button.Group>
        <Transition.Group
          as={List}
          duration={200}
          divided
          size='large'
          verticalAlign='middle'
        >
          {items1.map(item=> (
            <List.Item key={item}>
                <Dropdown placeholder='Select Side' value={member} fluid selection options = {sidesO} onChange={(e,{value})=>this.handleChangeS(value, 'member')} />  
                </List.Item>
          ))}
        </Transition.Group>
        <Button.Group>
          <Button compact disabled={items1.length === sides.length} onClick={this.handleAdd1}  ><Icon name='add circle'/>Add Side  </Button>
        </Button.Group>
        <br/>
        <div id = 'packbb'>
        <input type = "number" label='Number of People' placeholder='Number of People' onChange = {this.changeNum} />
        </div>
        
        <Form.TextArea label='Special Requests (if any)' placeholder='Special Request (Gluten Free, Nut Free, etc...) Some requests may add to total cost' name='message'  onChange = {this.addRequest} />
      
      <div >
       <Form>
        <Form.Field>
          How would you like to pay? <b>{this.state.value}</b>
        </Form.Field>
        <Form.Field>
          <Radio
            label='Online (a paypal invoice will be sent once order is confirmed)'
            name='radioGroup'
            value='Online'
            checked={this.state.value === 'Online'}
            onChange={this.handleChange}
          />
        </Form.Field>
        <Form.Field>
          <Radio
            label='In Person'
            name='radioGroup'
            value='In Person'
            checked={this.state.value === 'In Person'}
            onChange={this.handleChange}
          />
        </Form.Field>
      </Form>
      </div>
<br/>
Total Amount: $  { window.totalCost = this.state.people * this.packageVal} <br/>
      <div hidden>
       {window.requests = this.state.requests}
       {window.people = this.state.people}
       {window.pay = this.state.value}
       </div>
       <Link to='/orderConfirmation' style={{ fontWeight: 'bold' , color: 'white' }}>
        <Input type='submit' value="Submit" onClick = {this.onSubmit} />
        </Link>
              </Container>
        </Form>
            </center>
          </div>}
          </Transition.Group>
        </div>
        </div>
    )

              }}
