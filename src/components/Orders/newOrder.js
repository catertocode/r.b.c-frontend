import React, { Component } from 'react';
import {
    Button,
    Container,
    Divider,
    Grid,
    Header,
    Icon,
    Image,
    List,
    Menu,
    Segment,
    Visibility,
    Label,
  } from 'semantic-ui-react'

class newOrder extends Component{
    render() {
        return (
            <div>
              <h1>Pick A Package</h1>

                <Button id="Package1">
                    <h1>Package 1</h1>
                    <h2>Select 1 Entree</h2>
                    <h2>Select 2 Sides</h2>
                </Button>

                <Button id="Package2">
                    <h1>Package 2</h1>
                    <h2>Select 2 Entrees</h2>
                    <h2>Select 4 Sides</h2>
                </Button>

                <Button id="Package3">
                    <h1>Package 3</h1>
                    <h2>Select 4 Entrees</h2>
                    <h2>Select 6 Sides</h2>
                </Button>

               
            </div>
        );
    }
}

export default newOrder;