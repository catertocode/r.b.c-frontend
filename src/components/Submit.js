import React, { Component } from 'react'
import {
    Button,
    Container,
    Checkbox,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Image,
    Input,
    List,
    Menu,
    Message,
    Segment,
    Step,
    Visibility,
    Label,
} from 'semantic-ui-react'
import axios from 'axios';
import '../css/index.css';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'

export default class Submit extends Component {
    state = {}
    
    render() {
        const { name, email, phone, message } = this.state

        return (
            <div>
                <Visibility
                    onBottomPassed={this.showFixedMenu}
                    onBottomVisible={this.hideFixedMenu}
                    once={false}
                >
                    <Segment
                        inverted
                        textAlign='center'
                        style={{ padding: '1em 0em' }}
                        vertical
                    >

                        <Container>
                            <Step.Group unstackable>
                                <Step disabled>
                                    <Icon name='add'/>
                                    <Step.Content>
                                        <Step.Title>Basic Information</Step.Title>
                                        <Step.Description>Complete form with <p>required information.</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='calendar'/>
                                    <Step.Content>
                                        <Step.Title>Calendar</Step.Title>
                                        <Step.Description>Select date of <p>your event</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='target' />
                                    <Step.Content>
                                        <Step.Title>Package</Step.Title>
                                        <Step.Description>Choose desired package <p>and customize to your preference</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='info circle' />
                                    <Step.Content>
                                        <Step.Title>Confirm Order</Step.Title>
                                        <Step.Description>Verify your order details <p>to ensure accuracy</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step active>
                                    <Icon name='check'/>
                                    <Step.Content>
                                        <Step.Title>Order Placed</Step.Title>
                                        <Step.Description>You have successfully placed <p>an order!</p></Step.Description>
                                    </Step.Content>
                                </Step>
                            </Step.Group> <br/>
                        </Container>

                    </Segment>
                </Visibility>


                <div id='orderPage'>
                <Container text> <br/>

                    <div id = 'orderHeader'>
                        Congratulations {window.name},   <br/>
                        You have successfully placed an order with Roux Bayou Catering <br/> <br/>
                        </div>
                  <div id = 'orderDescriptions'>
                  If you chose to pay online, an invoice will be sent once your order has been manually confirmed by Roux Bayou. <br/><br/>
                    </div> <br/>
                   <div id = 'orderHeader'>
                       Thanks for your business! <br/>
                     </div>
               

                    <br/>

                </Container>
                </div>
            </div>
        )
    }
}