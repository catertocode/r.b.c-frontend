import React, { Component } from 'react'
import {
    Button,
    Container,
    Checkbox,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Image,
    Input,
    List,
    Menu,
    Message,
    Segment,
    Step,
    Visibility,
    Label,
} from 'semantic-ui-react'
import axios from 'axios';
import '../css/index.css';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'
var ent = 'butt'
var sid
//for(var i = 0; i < global.entree.length; i++){
  //  ent = ent + ' ' +  global.entree[i]+ ' ';
//}
for(var i = 0; i < 6; i++){
    ent = ent + ' ' +  global.entree[1] + ' ';
}
var name=window.name;
export default class orderConfirmation extends Component {
    constructor(){
        super();
        this.state={
            name: window.name,
            location1:window.location1,
            date:'',
            email:'',
            phone:'',
            entree:'', 
            side:'', 
            people:'',
            total:'', 
            requests:'',
            pay:''
        };
    }
    onSubmit = (e) =>{
        e.preventDefault();
        const {name= window.name,location1=window.location1,date=window.date,email=window.email,phone=window.phone,entree=window.entree1, side=window.side1, 
            people=window.people, total=window.totalCost, requests=window.requests, pay=window.pay} = this.state;

        axios.post('http://localhost:5000/api/orders',{
            name,location1,date,email,phone,entree, side, people, total, requests, pay : {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin" : "http://localhost:5000/api/orders", 
                        "Access-Control-Allow-Credentials" : true
         }     
        })
            .then((result)=>{

            });
    }
    onChange = (e)=>{
        const state = this.state
        state[e.target.name] = e.target.value;
        this.setState(state);
    }
    DisplayENT = () =>{
       
        }
    
    //handleChange = (e, { name, value }) => this.setState({ [name]: value })
    //handleSubmit = () => this.setState({ email: '', name: '', phone: '', message: '' })

    render() {
        const { name, email, phone, message } = this.state

        return (
            <div>
                <Visibility
                    onBottomPassed={this.showFixedMenu}
                    onBottomVisible={this.hideFixedMenu}
                    once={false}
                >
                    <Segment
                        inverted
                        textAlign='center'
                        style={{ padding: '1em 0em' }}
                        vertical
                    >

                        <Container>
                            <Step.Group unstackable>
                                <Step disabled>
                                    <Icon name='add'/>
                                    <Step.Content>
                                        <Step.Title>Basic Information</Step.Title>
                                        <Step.Description>Complete the form with <p>the required information.</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='calendar'/>
                                    <Step.Content>
                                        <Step.Title>Calendar</Step.Title>
                                        <Step.Description>Select date of <p>your event</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='target' />
                                    <Step.Content>
                                        <Step.Title>Package</Step.Title>
                                        <Step.Description>Choose desired package <p>and customize to your preference</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step active>
                                    <Icon name='info circle' />
                                    <Step.Content>
                                        <Step.Title>Confirm Order</Step.Title>
                                        <Step.Description>Verify your order details <p>to ensure accuracy</p></Step.Description>
                                    </Step.Content>
                                </Step>
                                <Step disabled>
                                    <Icon name='check'/>
                                    <Step.Content>
                                        <Step.Title>Order Placed</Step.Title>
                                        <Step.Description>You have successfully placed <p>an order!</p></Step.Description>
                                    </Step.Content>
                                </Step>
                            </Step.Group> <br/>
                        </Container>

                    </Segment>
                </Visibility>


                <div id='orderPage'>
                <Container text> <br/>

                    <div id = 'orderHeader'>
                        PLEASE CONFIRM YOUR ORDER: </div> <br/>
                <form onSubmit={this.onSubmit}>
                    <div id='orderDescriptions'>
                    
                            <p name="name" value={window.name} onChange={this.onChange} >Name: {window.name}</p>
                            <p>Phone Number: {window.phone}</p>
                            <p>Email: {window.email}</p>
                            <p>Location: {window.location1}</p>
                            <p>Date: {window.date}</p>
                            <p>Entrees: {window.entree1}</p>
                            <p>Sides: {window.side1}</p>
                            <p>Amount of people: {window.people}</p>
                            <p>Special Requests (if any) : {window.requests}</p>
                            <p>Total Cost: $ {window.totalCost}</p>
                            <p> I'd like to pay: {window.pay}</p>
                            </div>
                           
                            <br/>       
                            
                    <br/><br/>

                    <center>
                    <Button type="submit" color='blue' size='huge'><Link to='/submit' style={{ fontWeight: 'bold' , color: 'white' }}>CONFIRM</Link></Button>
                    </center>
                    </form>
                    <br/>
                
                </Container>
                </div>
            </div>
        )
    }
}