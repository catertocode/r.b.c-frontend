import React, { Component } from 'react'
import {
    Button,
    Container,
    Checkbox,
    Divider,
    Form,
    Grid,
    Header,
    Icon,
    Image,
    Input,
    List,
    Menu,
    Message,
    Segment,
    Step,
    Visibility,
    Label,
} from 'semantic-ui-react'
import '../css/index.css';
import { Link } from "react-router-dom";
import EventInquiry from './EventInquiry'
import { Switch, Route } from 'react-router-dom'

export default class orderWelcome extends Component {
    state = {}

    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleSubmit = () => this.setState({ email: '', name: '', phone: '', message: '' })

    render() {

        const { name, email, phone, message } = this.state

        return (
            <div id='orderPage'>
                <Visibility
                    onBottomPassed={this.showFixedMenu}
                    onBottomVisible={this.hideFixedMenu}
                    once={false}
                >
                    <Segment
                        inverted
                        textAlign='center'
                        style={{ padding: '1em 0em' }}
                        vertical
                    >

                        <Container text>
                            <Header
                                as='h2'
                                id='headerTextFadeIn'
                                content='Thank you for choosing Roux Bayou Catering!'
                                inverted
                                style={{ fontSize: '3.50em', fontWeight: 'normal', marginBottom: '1em', marginTop: '0em' , fontFamily: 'Baskerville Old Face'}}
                            />

                        </Container>

                    </Segment>
                </Visibility>

<br/>
                <Container text> <br/>
                    <div id = 'orderHeader'>
                        ORDERING PROCESS: </div> <br/>
                    <div id='orderDescriptions'>
                        <p> We are excited that you've chosen us to cater your special occasion. </p>
                        <p> We would like to remind you that there is a 10 person minimum for all orders.</p>
                    <p> To begin the ordering process, please complete the form with the required
                        information, such as your name, phone number, email, and location of event. </p>

                    <p> Upon completion of the form, you will be able to select the date of your
                        event from our availability calendar. Once you have chosen an available date,
                        proceed to select a catering package most suitable for your occasion,
                        then customize the order to your preference. </p>

                    <p> Before submitting your order, please review your order to ensure everything is
                        correct. If you are satisfied, submit your order, and you have successfully
                        completed the ordering process! We will review your request as soon as, and
                        contact you to provide the status, along with anything relating to your order.</p></div>

                        <br/><br/>

                    <center>
                        <Button color='blue' size='huge'><Link to='/information' style={{ fontWeight: 'bold' , color: 'white' }}>Start Your Order</Link></Button>
                    </center>

                    <br/>

                </Container>
            </div>
        )
    }
}